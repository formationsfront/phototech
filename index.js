const express = require("express");
const session = require("express-session");
const fileUpload = require("express-fileupload");
const flash = require('connect-flash');
const mongoose = require("mongoose");

const path = require("path");

const AlbumRoutes = require("./routes/album.routes");


const app = express();
mongoose.connect("mongodb://localhost:27017/phototheque");

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(fileUpload());
app.use(flash());
app.use(
  session({
    secret: "KkXEIB@E;AVQg:+8!?TmCc~t#z?Uplyb|_;8]{&$5:s?44%#}H$(8,bA2S%oYB_",
    resave: false,
    saveUninitialized: true
  })
);



app.get('/', (req, res) => {
  res.redirect('/albums');
});

app.use("/", AlbumRoutes);
app.use((req, res) => {
  res.status(404).send("Page not found");
});
app.use((error, req, res, next) => {
  console.error(error);
  res.status(500).send("An error occurred");
});
app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
