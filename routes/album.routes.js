const e = require("express");
const express = require("express");
const router = express.Router();
const AlbumController = require("../controllers/album.controller");



router.get("/albums", AlbumController.albumsList);

router.get("/albums/create", AlbumController.createAlbumForm);

router.post("/albums/create", AlbumController.createAlbum); 

router.get("/albums/:id", AlbumController.albumsView);

router.post("/albums/:id/images", AlbumController.addImageToAlbum);

router.get("/albums/:id/delete/:imageIndex", AlbumController.deleteImageToAlbum);

router.get("/albums/:id/delete/", AlbumController.deleteAlbum);

module.exports = router;