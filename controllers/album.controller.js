const Album = require("../models/Album");
const path = require("path");
const fs = require("fs");
const rimraf = require("rimraf");

const albumsList = async (req, res) => {
  const albums = await Album.find();
  res.render("albums", { title: "Mes albums photos", albums });
};

const albumsView = async (req, res) => {
  try {
  const album = await Album.findById(req.params.id);
  res.render("album", { title: album.title, album , error: req.flash("error")});
  }
  catch (error) {
    console.log(error);
    res.redirect("/404");
  }
};

const createAlbumForm = (req, res) => {
  res.render("new-album", { title: "Nouvel Album", error: req.flash("error")});
};
const createAlbum = async (req, res) => {
  try {
    if(!req.body.title) {
      req.flash("error", "Le titre est obligatoire");
      return res.redirect("/album/create");
    }
    await Album.create({
      title: req.body.title,
      description: req.body.description,
    });
    res.redirect("/albums");
  } catch (error) {
    req.flash("error", "Erreur lors de la création de l'album");
    res.redirect("/album/create");
  }
};
const addImageToAlbum = async (req, res) => {
  
  const idAlbum = req.params.id;
  const album = await Album.findById(idAlbum);

  if (!req?.files?.images) {
    req.flash("error", "Aucun fichier n'a été envoyé");
    return res.redirect(`/albums/${idAlbum}`);
  }

  const images = Array.isArray(req.files.images) ? req.files.images : [req.files.images];

  for (const image of images) {
    if (!image.mimetype.startsWith("image")) {
      req.flash("error", "Le fichier envoyé n'est pas une image");
      return res.redirect(`/albums/${idAlbum}`);
    }

    const uploadDir = path.join(__dirname, "..", "public", "uploads", idAlbum);
    fs.mkdirSync(uploadDir, { recursive: true });
    const localPath = path.join(uploadDir, image.name);
    await image.mv(localPath);

    album.images.push(image.name);
  }

  await album.save();
  res.redirect(`/albums/${idAlbum}`);
};

const deleteImageToAlbum = async (req, res) => {
  const idAlbum = req.params.id;
  const album = await Album.findById(idAlbum);
  const imageIndex = req.params.imageIndex;
  const image = album.images[imageIndex];

  console.log(image)
  if(!image) {
    req.flash("error", "L'image n'existe pas");
    return res.redirect(`/albums/${idAlbum}`);
  }
  album.images.splice(imageIndex, 1);
  await album.save();
  const imagePath = path.join(__dirname, "../public/uploads", idAlbum, image);
  fs.unlinkSync(imagePath);
  
  return res.redirect(`/albums/${idAlbum}`);
}
const deleteAlbum = async (req, res) => {
  const idAlbum = req.params.id;
  const album = await Album.findByIdAndDelete(idAlbum);
  const uploadDir = path.join(__dirname, "..", "public", "uploads", idAlbum);

  // On utilise la fonction callback de rimraf pour s'assurer que le dossier est bien supprimé avant de rediriger
  // la librairie ne propose pas de fonction asynchrone
  rimraf.sync(uploadDir);
    console.log("Dossier supprimé");
    res.redirect("/albums");
 
}
module.exports = {
  createAlbumForm,
  createAlbum,
  albumsList,
  albumsView,
  addImageToAlbum,
  deleteImageToAlbum,
  deleteAlbum,
};
