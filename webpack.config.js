// webpack.config.js
const path = require("path");

module.exports = {
  mode: "development",
  entry: "./assets/index.js", // le point d'entrée de votre application
  output: {
    filename: "main.js", // le nom du fichier de sortie
    path: path.resolve(__dirname, "public/dist"), // le chemin du dossier de sortie
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"], // pour gérer les fichiers CSS
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource", // pour gérer les fichiers d'images
      },
    ],
  },
};
